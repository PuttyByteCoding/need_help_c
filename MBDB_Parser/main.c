#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdint.h> // gives access to uint8_t, uint32_t, etc
#include <stdbool.h> // gives access to bool types
#include <string.h>

#pragma pack(push, 1)

//Structs go here
struct DynamicArrayOfChars {
    int arraySize;
    char *theArray;
};


#pragma pack(pop)

uint16_t byteSwap_uint16_t(uint16_t orig);
uint32_t byteswap_uint32_t(uint32_t orig);

int main(int argc, char *argv[]) {
    //Open the SQLite file
    FILE *ptr_ManifestMbdbFile;
    ptr_ManifestMbdbFile = fopen("/Users/mark/Desktop/Samples/iOS_Backup_Samples/Manifest.mbdb", "rb");
    //Confirm file opened, write error if file failed to open
    if (!ptr_ManifestMbdbFile)
    {
        printf("\nUnable to open file!\n\n");
        return 1;
    }
    else {
        printf("\nOpened file \n");
    }
    
    fseek(ptr_ManifestMbdbFile, 6, SEEK_SET);
    
    
    uint16_t stringLen;
    fread(
          &stringLen,		// Memory address to put results of data read
          sizeof(stringLen), // Number of bytes per block
          1,							// How many blocks to read
          ptr_ManifestMbdbFile);			// File Variable to read from
    stringLen = byteSwap_uint16_t(stringLen);
    
    printf("\nString Length is: %04X", stringLen);
    

    struct DynamicArrayOfChars thisCharsArray;
    thisCharsArray.arraySize = stringLen+1;
    thisCharsArray.theArray = (char*)malloc(sizeof(char) * thisCharsArray.arraySize);
    
    fread(
          &thisCharsArray.theArray,		// Memory address to put results of data read
          stringLen, // Number of bytes per block
          1,							// How many blocks to read
          ptr_ManifestMbdbFile);			// File Variable to
    
    printf("\nDomain is: %s", &thisCharsArray.theArray );
    
    
    printf("\n\nComplete!!!\n\n");
    
    return 0;
}

uint16_t byteSwap_uint16_t(uint16_t orig) {
    uint16_t byte1 = (orig & 0xff00) >> 8;
    uint16_t byte2 = (orig & 0xff);
    uint16_t results = byte2 << 8 | byte1;
    return results;
}

uint32_t byteSwap_uint32_t(uint32_t orig) {
    uint32_t byte1 = (orig & 0xff000000) >> 24;
    uint32_t byte2 = (orig & 0x00ff0000) >> 16;
    uint32_t byte3 = (orig & 0x0000ff00) >> 8;
    uint32_t byte4 = (orig & 0x000000ff);
    uint32_t results = byte4 << 24 | byte3 << 16 | byte2 << 8 | byte1;
    return results;
}
